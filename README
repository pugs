This used to be the pugs repository, but is nowadays a generic repository
for hosting anything related to Perl 6. In particular, it contains the synopsis
(specification), test suite, standard grammar, some implementations like SMOP,
mildew, v6 and elf, various Perl 6 examples, documentation efforts and examples.

The sources for the Pugs Perl 6 compiler can be found at
<http://github.com/audreyt/Pugs.hs>.

If you have any questions, don't hesitate to to ask on the
irc://irc.freenode.org/#perl6 IRC channel, or send an email to either
perl6-users@perl.org or perl6-compilers.org. Do the same to obtain a commit
bit for this repository.

Once you have commit access, you can also invite others via the web interface
at <http://commitbit.pugscode.org/admin/project/Pugs/people>.

Many outdated things have been removed from this repository. If you still want
to see them, check out revision r30211, which was shortly before the big
cleaning.

Policy
======

We are rather liberal with what you can do here.
The only limitations are: be friendly, stay on topic (projects in here should
be related to Perl 6 in some way), only put free software in here, 

All code and documentation in this repository is considered as shared, so you
are free to improve and enhance other people's code, and you should be open to
contributions from others.
